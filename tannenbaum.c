//
// Created by Renick Büttner on 2018-12-10.
// Compile: cc tannenbaum.c -o tree -std=c90 && ./tree
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_BROWN    "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define LEAF "%"
#define clear() printf("\033[H\033[J")
#define goToXY(x,y) printf("\033[%d;%dH", (x), (y))

char * getTopping();
char * getTree();
char * getDelimiter();

int main() {
    int i = 0;

    do {
        clear();
        goToXY(0, 0);
        printf(ANSI_COLOR_GREEN);
        printf("\n%s", getDelimiter());
        printf("%s", getTopping());
        printf("%s", getTree());
        printf("%s", getDelimiter());
        sleep(1);
        i++;
    } while (i < 30);

    printf(ANSI_COLOR_YELLOW);
    printf(" ~ ~ Frohe Weihnachten! ~ ~\n");
    printf(ANSI_COLOR_RESET);
    return 0;
}

char * getDelimiter() {
    char * res = malloc(4);
    strcpy(res, "\n");
    return res;
}

char * getTopping() {
    char * res = malloc(160);
    strcpy(res, ANSI_COLOR_RED);

    srand(time(NULL));
    int i = (rand() % 4) + 1;

    if(i == 4) strcat(res, "           \\ /\n           <=>\n          <===>");
    if(i == 3) strcat(res, "\n            O\n           o=o");
    if(i == 2) strcat(res, "           \\ /\n            *\n           ***");
    if(i == 1) strcat(res, "          `   ´\n           \\ /\n            U");

    strcat(res, ANSI_COLOR_GREEN);

    return res;
}

char * getTree() {
    char * res = malloc(160);
    strcpy(res, "");

    int i    = -1, // row iterator
        j    = 0,  // column iterator
        rows = 12,
        prev = 0, // counter for space between decorations
        prevItem = 0; // number of previous item

    // generating the loops
    while(++i<rows) {

        // This loop will print Spaces before '*' on each row
        for(j=-2;++j<rows-i;) strcat(res, " ");

        // This loop will print * on each row
        for(j=0;++j<2*i;) {

            // watch the border
            if(j > 2 && j < (2*i - 2) && i < 11) {
                if (prev > 2) prev = 0;
                switch((rand() % 5) + 1) { // different types of decorations
                    case 1:
                        if(prev == 0 && prevItem != 1) {
                            strcat(res, ANSI_COLOR_BLUE);
                            strcat(res, "#");
                            strcat(res, ANSI_COLOR_GREEN);
                            prevItem = 1;
                        } else strcat(res, LEAF);
                        break;
                    case 2:
                        if(prev == 0 && prevItem != 2) {
                            strcat(res, ANSI_COLOR_RED);
                            strcat(res, "*");
                            strcat(res, ANSI_COLOR_GREEN);
                            prevItem = 2;
                        } else strcat(res, LEAF);
                        break;
                    case 3:
                        if(prev == 0 && prevItem != 2) {
                            strcat(res, ANSI_COLOR_CYAN);
                            strcat(res, "+");
                            strcat(res, ANSI_COLOR_GREEN);
                            prevItem = 2;
                        } else strcat(res, LEAF);
                        break;
                    case 4:
                        if(prev == 0 && prevItem != 5) {
                            strcat(res, ANSI_COLOR_MAGENTA);
                            strcat(res, "o");
                            strcat(res, ANSI_COLOR_GREEN);
                            prevItem = 5;
                        } else strcat(res, LEAF);
                        break;
                    default:
                        strcat(res, LEAF);
                        break;
                }
                prev++;
            } else
                strcat(res, LEAF);
        }

        strcat(res, "\n");
    }

    strcat(res, ANSI_COLOR_BROWN);
    strcat(res, "          `[X]´\n");
    strcat(res, "           [X]\n");

    return res;
}